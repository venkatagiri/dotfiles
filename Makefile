STOW := stow --verbose --ignore Makefile --ignore Readme.md --target $$HOME

clean-existing-files:
	[ ! -L $$HOME/.bashrc ] && mv $$HOME/.bashrc $$HOME/.bashrc.bak || true

try:
	$(STOW) --no .

install: clean-existing-files
	$(STOW) .

install-without-stow:
	find . -type f | \
	egrep -v ".git|Makefile|Readme" |  \
	while read file; do \
		[ -L $$HOME/$$file ] && echo "already exists: $$file" && continue; \
		[ -f $$HOME/$$file ] && mv $$HOME/$$file $$HOME/$$file.bak; \
		mkdir -v -p $$HOME/$$(dirname $file); \
		ln -s $$(realpath --relative-to="$${HOME}/$$(dirname $$file)" $$PWD/$$file) $$HOME/$$file; \
	done
