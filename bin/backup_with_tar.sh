#! /bin/bash -eux

main() {
  local tar_file=/tmp/backup_$(hostname)_$(date +%Y%m%d%H%M%S).tar.gz

  tar --gzip --create --verbose \
    --file $tar_file \
    --exclude '.asdf' --exclude '.cache' --exclude 'cache' --exclude '.npm' --exclude 'downloads' \
    /home /etc /usr/local /opt \
    || true
  su reddy --command "rsync -avzP $tar_file /mnt/vault/backup/$(hostname)/."
  rm $tar_file
}

main "$@"
