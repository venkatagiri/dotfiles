#!/bin/bash

##### environment
# set editor to vim
export EDITOR=vim

# add private bin to PATH
[[ -d "$HOME/bin" ]] && PATH="$HOME/bin:$PATH"

# load cargo env
[[ -f "$HOME/.cargo/env" ]] && source $HOME/.cargo/env

# load teleport commands
[[ -f "$HOME/.teleport" ]] && source $HOME/.teleport

# load asdf vm
if [[ -d "$HOME/.asdf" ]] ; then
  source $HOME/.asdf/asdf.sh
  source $HOME/.asdf/completions/asdf.bash
fi


##### if not running interactively, don't setup shell
[[ $- = *i* ]] || return


##### shell look & feel
shopt -s autocd
shopt -s globstar
shopt -qs checkwinsize
bind 'set mark-symlinked-directories on'

# better prompt
cd () { builtin cd "$@" && chpwd; }
chpwd () {
  case $PWD in
    $HOME) HPWD="~";;
    $HOME/*/*) HPWD="${PWD#"${PWD%/*/*}/"}";;
    $HOME/*) HPWD="~/${PWD##*/}";;
    /*/*/*) HPWD="${PWD#"${PWD%/*/*}/"}";;
    *) HPWD="$PWD";;
  esac
}
chpwd
PS1='\[\e[0;31m\]\u\[\e[m\]@\[\e[0;36m\]\h\[\e[m\] $HPWD \[\e[1;32m\]$\[\e[m\] '


##### shell history
shopt -s histappend
HISTCONTROL=ignoreboth
HISTFILESIZE=
HISTSIZE=
HISTTIMEFORMAT="[%F %T] "
HISTFILE=$HOME/.bash_eternal_history
PROMPT_COMMAND="history -a; $PROMPT_COMMAND"


##### aliases
alias ls='ls --color'
alias l='ls -lrth'
alias ll='ls -lA'
alias la='ls -lart'
alias lr='ls -lAR'
alias pg='ps -eaf | grep -v grep | grep'
alias sr='screen -r'
alias ss='screen -S'
alias sl='screen -ls'
alias d='df -khT | egrep -v "tmpfs|udev|overlay|squashfs"'
alias t='top -d1'
alias ip='ip -br -c'
alias sudo='sudo '
alias diff='diff -ru'
alias cp='cp -v'
alias mv='mv -v'
alias rm='rm -v'
alias rsync='rsync --progress --itemize-changes'
alias rclone='rclone -vv --progress'
alias sc='sudo supervisorctl'
scl() (tail -50f /var/log/supervisor/$1*)
ff() (ffmpeg -i "${1:?'missing input operand'}" -c copy -strict -2 "${2:?'missing output operan'}")

# docker
dc() (cd $HOME/projects/thebox; docker-compose -f docker-compose.$(hostname).yml "$@")
dcrc() (dc stop $@ && dc rm -f $@ && dc up -d --no-recreate $@)
dclogs() (dc logs --tail 10 -f $@)
alias dps='docker ps --format "table {{.ID}}\t{{.Names}}\t{{.Image}}\t{{.Command}}\t{{.Size}}\t{{.Status}}"'

# git
alias ga='git add'
alias gp='git push'
alias gpa='git pushall'
alias gl='git log --oneline'
alias glh='git log --oneline | head'
alias gs='git status'
alias gd='git diff'
alias gdc='git diff --cached'
alias gc='git commit -m'
alias gac='git commit -am'
alias gb='git branch'

[[ -f $HOME/.bash_aliases ]] && source $HOME/.bash_aliases


##### enable programmable completion features
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
