execute pathogen#infect()
syntax on

filetype plugin indent on

se expandtab autoindent hidden hlsearch
se ts=2 sw=2
se colorcolumn=120
se dir=~/.vim/tmp
color desert

autocmd FileType html setlocal ts=2 sts=2 sw=2
autocmd FileType javascript setlocal ts=4 sts=4 sw=4
autocmd FileType ruby setlocal ts=2 sts=2 sw=2
autocmd FileType rust setlocal ts=4 sts=4 sw=4
autocmd FileType yaml setlocal ts=2 sts=2 sw=2

let g:rustfmt_autosave = 1

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" make run
map <F2> :wa<CR>:!make run<CR>
map <F3> :wa<CR>:!make test<CR>
map <F5> :wa<CR>:!./%<CR>
