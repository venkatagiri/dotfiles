#!/bin/bash

# if running bash, load bashrc
[[ -n "$BASH_VERSION" ]] && [[ -f "$HOME/.bashrc" ]] && source $HOME/.bashrc

## ssh-agent
#if [[ ! -S $HOME/.ssh/ssh_auth_sock ]]; then
#  eval $(ssh-agent)
#  ln -sf "$SSH_AUTH_SOCK" $HOME/.ssh/ssh_auth_sock
#fi
#export SSH_AUTH_SOCK=$HOME/.ssh/ssh_auth_sock
#ssh-add -l | grep "The agent has no identities" && ssh-add || true
